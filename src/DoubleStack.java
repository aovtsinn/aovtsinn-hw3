import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack 
{
	private LinkedList<Double> stack =  new LinkedList<>();
	public static void main (String[] argum) {
		System.out.println(interpret("3 x"));
		}
   DoubleStack() {
	   stack = new LinkedList<Double>();
   }
   @SuppressWarnings("unchecked")
@Override
   public Object clone() throws CloneNotSupportedException 
   { 
	   DoubleStack clonedstack = new DoubleStack();
	   clonedstack.stack = (LinkedList<Double>) stack.clone();
	   return clonedstack; 
	   }

   public boolean stEmpty() 
   {
	   return stack.isEmpty(); 
	   }

   public void push (double a) {
	   this.stack.push(a);
   }

   public double pop() {
	   if (stack.isEmpty()) {
			   throw  new  IndexOutOfBoundsException("The stack is Empty");
		}
	   return this.stack.pop();
   }

   public void op (String s) {
	   if (stack.size() < 2 ) {
		   throw new RuntimeException("Too few elements to do" + s);	
	}
	   double second = stack.pop();
       double first = stack.pop();
       switch(s) {
          case "+":
             stack.push(first + second);
             break;
          case "-":
             stack.push(first - second);
             break;
          case "*":
             stack.push(first * second);
             break;
          case "/":
             stack.push(first / second);
             break;
          default:
        	  throw new RuntimeException("The operation "+ s +"is invalid.");
       }
   }
   public double tos() {
	   if (stEmpty()) {
			throw new IndexOutOfBoundsException("There is no elements" + stack + "given");
		}
	   return stack.element();
   }
   @Override
   public boolean equals (Object o) {
	   return this.stack.equals(((DoubleStack)o).stack);
   }
   @Override
   public String toString() {
	      return super.toString();
	   }
   
   
   
   public static double interpret (String pol) {
	   if (pol.trim().isEmpty() || pol == null) {
			  throw new RuntimeException ("Error 503: Input" + pol + "cannot be empty!");
		  }
	   String stackStr;
	   StringTokenizer listTokenized = new StringTokenizer(pol, " \t");
	   DoubleStack doubleStack = new DoubleStack();
	   int j = 1;
	   int counter = listTokenized.countTokens();
	   while (listTokenized.hasMoreTokens()) {
		    stackStr = (String) listTokenized.nextElement();
		    if (stackStr.equals("-") || stackStr.equals("+") || stackStr.equals("/") || stackStr.equals("*")) 
		    	doubleStack.op(stackStr);
			else {
				if (counter == j && j > 2) 
				throw new RuntimeException("Error in operation with" + pol + " Counter" + counter + "no enough");
				try {
					double a = Double.parseDouble(stackStr);
	   				doubleStack.push(a);
	   				}
				catch(NumberFormatException e) {  
	   				throw new RuntimeException("Expression with \" " + pol + " \" is not valid");
   				}  
   			}
   		j++;
   		}
   	return doubleStack.tos();	 
}
}